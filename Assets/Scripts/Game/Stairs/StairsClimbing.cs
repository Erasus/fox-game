﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Rigidbody2D))]
public class StairsClimbing : MonoBehaviour
{
    [SerializeField] private Collider2D _groundCollider2D;
    [SerializeField] private float _speed;

    private Animator _animator;
    private Rigidbody2D _rigidbody2D;
    private float _standartGravity;

    public bool Climb { get; private set; }

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _standartGravity = _rigidbody2D.gravityScale;
    }

    private void Update()
    {
        if (Climb)
        {
            if (Input.GetKey(KeyCode.W))
            {
                _rigidbody2D.velocity = new Vector2(0, _speed);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                _rigidbody2D.velocity = new Vector2(0, -_speed);
            }
            else
            {
                _rigidbody2D.velocity = new Vector2(0, 0);
            }

            _animator.SetFloat("VerticalSpeed", Mathf.Abs(_rigidbody2D.velocity.y));        // new
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Stairs>(out Stairs stairs))
        {
            _animator.SetTrigger("StartClimb");
            Climb = true;
            _animator.SetBool("Climb", Climb);
            _rigidbody2D.gravityScale = 0;
            _rigidbody2D.velocity = new Vector2(0, 0);
            _groundCollider2D.enabled = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Stairs>(out Stairs stairs))
        {
            Climb = false;
            _animator.SetBool("Climb", Climb);
            _rigidbody2D.gravityScale = _standartGravity;
            _groundCollider2D.enabled = true;
        }
    }
}
