﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class GroundCheck : MonoBehaviour
{
    [SerializeField] private Collider2D _groundCollider2D;
    [SerializeField] private LayerMask _whatIsGround;

    private Animator _animator;

    public bool IsGrounded { get; private set; }
    public bool OnPlatform { get; private set; }

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CheckGround();
        
        if(collision.TryGetComponent(out Platform platform))
            OnPlatform = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CheckGround();
        
        if (collision.TryGetComponent(out Platform platform))
            OnPlatform = false;
    }

    private void CheckGround()
    {
        IsGrounded = _groundCollider2D.IsTouchingLayers(_whatIsGround);
        _animator.SetBool("IsGround", IsGrounded);
    }
}
