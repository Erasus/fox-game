﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class PlayerMove : MonoBehaviour
{
    [Header("Переменные для движения")]
    [SerializeField] private float _speed;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private float _moveHorizontal;
    private bool _lookFaceLeft = false;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }
    private void FixedUpdate()
    {
        _moveHorizontal = Input.GetAxisRaw("Horizontal");

        _rigidbody2D.velocity = new Vector2(_moveHorizontal * _speed, _rigidbody2D.velocity.y);

        if (_lookFaceLeft == false && _moveHorizontal > 0)
            Flip();
        else if (_lookFaceLeft == true && _moveHorizontal < 0)
            Flip();

        _animator.SetFloat("Speed", Mathf.Abs(_moveHorizontal));
    }

    private void Flip()
    {
        _lookFaceLeft = !_lookFaceLeft;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}
