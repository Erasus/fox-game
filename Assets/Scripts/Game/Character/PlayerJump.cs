﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GroundCheck), typeof(Rigidbody2D), typeof(Animator))]
public class PlayerJump : MonoBehaviour
{
    [SerializeField] private CapsuleCollider2D _bodyCollider2D;
    [Header("Переменные для прыжка")]
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _jumpTimeCounter;
    [SerializeField] private float _jumpTime;
    [SerializeField] private UnityEvent _jumpSound;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private GroundCheck _groundCheck;
    private bool _isJumping;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _groundCheck = GetComponent<GroundCheck>();
    }

    private void FixedUpdate()
    {
        if (!_groundCheck.IsGrounded)
        {
            _animator.SetFloat("VerticalSpeed", _rigidbody2D.velocity.y);
        }

        if (_rigidbody2D.velocity.y <= 0.01f)
            _animator.SetBool("Fall", true);
        else
            _animator.SetBool("Fall", false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _groundCheck.IsGrounded == true)
        {
            Jump();
        }

        if (Input.GetKey(KeyCode.Space) && _isJumping == true)
        {
            if (_jumpTimeCounter > 0)
            {
                _rigidbody2D.velocity = Vector2.up * _jumpForce;
                _jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                _isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            _isJumping = false;
        }
    }

    private void Jump()
    {
        if (Input.GetKey(KeyCode.S) && _groundCheck.OnPlatform)
        {
            StartCoroutine(JumpDownPlatform());
        }
        else
        {
            _isJumping = true;
            _jumpTimeCounter = _jumpTime;
            _rigidbody2D.velocity = Vector2.up * _jumpForce;
        }
        
        _jumpSound?.Invoke();
    }

    private IEnumerator JumpDownPlatform()
    {
        _bodyCollider2D.enabled = false;
        yield return new WaitForSeconds(0.3f);
        _bodyCollider2D.enabled = true;
    }
}
