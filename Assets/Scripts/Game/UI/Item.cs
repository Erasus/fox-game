﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class Item : MonoBehaviour
{
    [SerializeField] private UnityEvent _reached;

    private Animator _animator;
    private bool _taked = false;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Player>(out Player player) && _taked == false)
        {
            _taked = true;
            _reached?.Invoke();
            _animator.SetTrigger("Collect");
        }
    }

    private void OnDestroy()
    {
        Destroy(gameObject);
    }
}
