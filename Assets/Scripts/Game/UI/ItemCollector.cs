﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemCollector : MonoBehaviour
{
    [SerializeField] private Container _foodContainer;
    [SerializeField] private Container _treasureContainer;
    [SerializeField] private TMP_Text _foodCount;
    [SerializeField] private TMP_Text _treasureCount;
    [SerializeField] private TMP_Text _maxFoodCount;
    [SerializeField] private TMP_Text _maxTreasureCount;

    private int _food = 0;
    private int _treasure = 0;
    private int _maxFood;
    private int _maxTreasure;

    private void Start()
    {
        _maxFood = _foodContainer.GetComponentsInChildren<Item>().Length;
        _maxTreasure = _treasureContainer.GetComponentsInChildren<Item>().Length;
        SetMaxValue();
    }

    public void OnTreasureCollect()
    {
        _treasure++;
        _treasureCount.text = _treasure + " / " + _maxTreasure;
    }
    
    public void OnFoodCollect()
    {
        _food++;
        _foodCount.text = _food + " / " + _maxFood;
    }

    private void SetMaxValue()
    {
        _treasureCount.text = _treasure + " / " + _maxTreasure;
        _foodCount.text = _food + " / " + _maxFood;
    }
}
