﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(GroundCheck), typeof(Rigidbody2D), typeof(Animator))]
public class FrogJump : MonoBehaviour
{
    [SerializeField] private JumpPosition _leftPosition;
    [SerializeField] private JumpPosition _rightPosition;
    [Header("Переменные для прыжка")]
    [SerializeField] private float _jumpForce;
    [SerializeField] private int _jumpCount;
    [SerializeField] private int _duration;
    [SerializeField] private float _startTimeBetweenJump = 3;
    [SerializeField] private float _timeBetweenJump = 1;

    private Rigidbody2D _rigidbody2D;
    private GroundCheck _groundCheck;
    private Animator _animator;
    private bool _directionJump;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _groundCheck = GetComponent<GroundCheck>();
    }

    private void Update()
    {
        if(_groundCheck.IsGrounded == false)
            _animator.SetFloat("VerticalSpeed", _rigidbody2D.velocity.y);

        if (_timeBetweenJump <= 0 && _groundCheck.IsGrounded == true)
        {
            if(transform.position.x > _leftPosition.transform.position.x)
            {
                if(_directionJump)
                {
                    Flip();
                    Jump(_leftPosition.transform);
                }
                else
                {
                    Jump(_leftPosition.transform);
                }
                
            }
            else
            {
                Flip();
                Jump(_rightPosition.transform);
            }
            
            _timeBetweenJump = _startTimeBetweenJump;
        }
        else
        {
            _timeBetweenJump -= Time.deltaTime;
        }
    }

    private void Flip()
    {
        _directionJump = !_directionJump;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    private void Jump(Transform transformPosition)
    {
        transform.DOJump(transformPosition.transform.position, _jumpForce, _jumpCount, _duration);
    }
}
