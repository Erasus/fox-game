﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class Patrol : MonoBehaviour
{
    [Header("Точки маршрута")]
    [SerializeField] private WalkPoint _leftArea;
    [SerializeField] private WalkPoint _rightArea;

    [Space(5)]
    [Header("Переменные для движения")]
    [SerializeField] private float _speed;

    private bool _turnAround;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (transform.position.x > _rightArea.transform.position.x)
            _turnAround = false;
        else if (transform.position.x < _leftArea.transform.position.x)
            _turnAround = true;

        _rigidbody2D.velocity = _turnAround ? Vector2.right : Vector2.left;
        _rigidbody2D.velocity *= _speed;

        if (_rigidbody2D.velocity.x > 0)
            _spriteRenderer.flipX = true;

        if (_rigidbody2D.velocity.x < 0)
            _spriteRenderer.flipX = false;
    }
}
