﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLimiters : MonoBehaviour
{
    [SerializeField] private float _top;
    [SerializeField] private float _bottom;
    [SerializeField] private float _left;
    [SerializeField] private float _right;
    
    private void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, _left, _right), Mathf.Clamp(transform.position.y, _bottom, _top), transform.position.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector2(_left, _top), new Vector2(_right, _top));
        Gizmos.DrawLine(new Vector2(_right, _top), new Vector2(_right, _bottom));
        Gizmos.DrawLine(new Vector2(_right, _bottom), new Vector2(_left, _bottom));
        Gizmos.DrawLine(new Vector2(_left, _bottom), new Vector2(_left, _top));
    }
}
