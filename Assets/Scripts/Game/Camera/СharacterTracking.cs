﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class СharacterTracking : MonoBehaviour
{
    [SerializeField] private float _dumping = 2f;
    [SerializeField] private Vector2 _offset;
    [SerializeField] private bool _isLeft;
    [SerializeField] private Transform _player;

    private int _lastLook;

    private void Start()
    { 
        _offset = new Vector2(Mathf.Abs(_offset.x), _offset.y);
        InitializePlayer(_player);
    }

    private void Update()
    {
        if (_player)
        {
            int currentX = Mathf.RoundToInt(_player.position.x);

            if (currentX > _lastLook)
                _isLeft = false;
            else if (currentX < _lastLook)
                _isLeft = true;

            _lastLook = Mathf.RoundToInt(_player.position.x);
            Vector3 target;

            if (_isLeft)
                target = new Vector3(_player.position.x - _offset.x, _player.position.y + _offset.y, transform.position.z);
            else
                target = new Vector3(_player.position.x + _offset.x, _player.position.y + _offset.y, transform.position.z);

            Vector3 currentPosition = Vector3.Lerp(transform.position, target, _dumping * Time.deltaTime);
            transform.position = currentPosition;
        }
    }

    private void InitializePlayer(bool playerIsLeft)
    {
        _lastLook = Mathf.RoundToInt(_player.position.x);

        if (playerIsLeft)
            transform.position = new Vector3(_player.position.x - _offset.x, _player.position.y + _offset.y, transform.position.z);   
        else
            transform.position = new Vector3(_player.position.x + _offset.x, _player.position.y + _offset.y, transform.position.z);
    }
}
