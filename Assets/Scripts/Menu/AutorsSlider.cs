﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutorsSlider : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _scrollSpeed;
    [SerializeField] private CanvasGroup _contentCanvasGroup;
    [SerializeField] private float _changeAlphaSpeed;

    private bool _climb = false;
    private bool _startScroll = false;

    private void Start()
    {
        _slider.value = _slider.maxValue;
        _contentCanvasGroup.alpha = 0;
    }
    private void Update()
    {
        ChangeAlpha();

        if(_startScroll)
        {
            _slider.value -= Time.deltaTime / _scrollSpeed;

            if (_slider.value >= _slider.maxValue || _slider.value <= 0)
                _climb = false;
            else
                _climb = true;

            _animator.SetBool("Climb", _climb);
        }
    }

    public void InitialValue()
    {
        _slider.value = _slider.maxValue;
    }

    private void ChangeAlpha()
    {
        if (_startScroll == false)
            _contentCanvasGroup.alpha += Time.deltaTime * _changeAlphaSpeed;

        if (_contentCanvasGroup.alpha >= 1)
            _startScroll = true;

        if(_startScroll == true && _slider.value <= 0)
            _contentCanvasGroup.alpha -= Time.deltaTime * _changeAlphaSpeed;
    }

    public void SetStandartAlphaValue()
    {
        _contentCanvasGroup.alpha = 0;
        _startScroll = false;
    }
}
